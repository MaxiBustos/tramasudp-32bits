#ifndef PACKET_H
#define PACKET_H

#include "alt_types.h"

int start_packet(void *base, int payload);
int stop_packet(void *base);
int is_packet_running(void *base);
int wait_until_packet_stops_running(void *base);


#endif /*PRBS_PACKET_H*/
