#ifndef PACKET_REGS_H
#define PACKET_REGS_H

#include "io.h"

// packet_evb accessor macros

#define PACKET_RD_CSR(base)                  IORD(base, 0)
#define PACKET_WR_CSR(base, data)            IOWR(base, 0, data)

#define PACKET_CSR_GO_BIT_MASK               (0x01)
#define PACKET_CSR_GO_BIT_OFST               (0)

#define PACKET_CSR_RUNNING_BIT_MASK          (0x02)
#define PACKET_CSR_RUNNING_BIT_OFST          (1)

#define PACKET_RD_BYTE_PAYLOAD(base)         IORD(base, 1)
#define PACKET_BYTE_PAYLOAD(base, data)      IOWR(base, 1, data)

#define RD_PACKET_COUNTER(base)       	     IORD(base, 3)
#define CLEAR_PACKET_COUNTER(base)           IOWR(base, 3, 0)


#endif /*PRBS_PACKET_REGS_H*/
