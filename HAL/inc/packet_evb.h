#ifndef PACKET_EVB_H
#define PACKET_EVB_H

#include "alt_types.h"

typedef struct {
    alt_u32 csr_state;      // csr value
    alt_u16 byte_count;     // byte length of generated packets
    alt_u32 packet_count;   // packet counter value
} PKT_GEN_STATS;

int start_packet_evb(void *base);
int stop_packet_evb(void *base);
int is_packet_evb_running(void *base);
int wait_until_packet_evb_stops_running(void *base);


#endif /*PRBS_PACKET_EVB_H*/
