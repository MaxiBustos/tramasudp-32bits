#include "packet_evb.h"
#include "packet_evb_regs.h"

//
// packet_evb utility routines
//

int start_packet_evb(void *base) {
    
    alt_u32 current_csr;

    // is the packet evb already running?
    current_csr = PACKET_EVB_RD_CSR(base);
    if(current_csr & PACKET_EVB_CSR_GO_BIT_MASK) {
        return 1;
    }
    if(current_csr & PACKET_EVB_CSR_RUNNING_BIT_MASK) {
        return 2;
    }
    
    // clear the counter    
    PACKET_EVB_CLEAR_PACKET_COUNTER(base);
    
    // write the parameter registers
    //PRBS_PACKET_GENERATOR_WR_BYTE_COUNT(base, byte_count);
    //PRBS_PACKET_GENERATOR_WR_INITIAL_VALUE(base, initial_value);
    
    // and set the go bit
    PACKET_EVB_WR_CSR(base, PACKET_EVB_CSR_RUNNING_BIT_MASK);
    
    return 0;
}

int stop_packet_evb(void *base) {
    
    // is the packet evb already stopped?
    if(!(PACKET_EVB_RD_CSR(base) & PACKET_EVB_CSR_GO_BIT_MASK)) {
        return 1;
    }

    // clear the go bit
    PACKET_EVB_WR_CSR(base, 0);
    
    return 0;
}

int is_packet_evb_running(void *base) {
    
    // is the packet evb running?
    if((PACKET_EVB_RD_CSR(base) & PACKET_EVB_CSR_RUNNING_BIT_MASK)) {
        return 1;
    }

    return 0;
}

int wait_until_packet_evb_stops_running(void *base) {
    
    // wait until packet evb stops running?
    while(is_packet_evb_running(base));

    return 0;
}